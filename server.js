const app = require('./index');

const server = app.listen(process.env.PORT || 8088, '0.0.0.0', () => {
  const { address, port } = server.address();
  console.log(`My app listening at http://${address}:${port}`);
});
