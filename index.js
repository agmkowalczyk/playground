const express = require('express');
const router = require('./routes');
const app = express();

app.get('/', (req, res) => {
  res.send('Hello World!')
});

app.use((req, res, next) => {
  console.log(`${req.method} ${req.url}`);
  next();
});

app.use('/', router);

module.exports = app;