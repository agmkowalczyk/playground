const express = require('express');
const router = express.Router();

const example = require('./scripts/example.js');
router.get('/api/example', example);

module.exports = router;